package edu.berkeley.cs160.woqytoqy.woqytoqy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

/**
 * Created by Sumer Mohammed on 12/4/2014.
 * THIS CLASS CORRESPONDS TO CAL VS. OREGON EVENT SCREEN
 */
public class PhoneComponent extends Activity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cal_vs_oregon);
        Button incident_log_button = (Button) findViewById(R.id.incident_log_button);
        Button requests_responses_button = (Button) findViewById(R.id.requests_responses_button);
        Button incidents_by_location_button = (Button) findViewById(R.id.incidents_by_location_button);
        Button incidents_over_time = (Button) findViewById(R.id.incidents_over_time_button);
        Button back_button = (Button) findViewById(R.id.back_button);

        incident_log_button.setOnClickListener(this);
        requests_responses_button.setOnClickListener(this);
        incidents_by_location_button.setOnClickListener(this);
        incidents_over_time.setOnClickListener(this);
        back_button.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.incident_log_button) {
            Intent next_intent = new Intent (getApplicationContext(),IncidentList.class);
            startActivity(next_intent);
        } else if (view.getId() == R.id.requests_responses_button) {
            Intent next_intent = new Intent (getApplicationContext(),RequestsResponses.class);
            startActivity(next_intent);
        } else if (view.getId() == R.id.incidents_by_location_button) {
            Intent next_intent = new Intent (getApplicationContext(),IncidentsByLocation.class);
            startActivity(next_intent);
        } else if (view.getId() == R.id.incidents_over_time_button) {
            Intent next_intent = new Intent (getApplicationContext(),IncidentsOverTime.class);
            startActivity(next_intent);
        } else if (view.getId() == R.id.back_button) {
            Intent next_intent = new Intent (getApplicationContext(),MainActivity.class);
            startActivity(next_intent);
        }
    }


}
