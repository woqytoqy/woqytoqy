package edu.berkeley.cs160.woqytoqy.woqytoqy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Sumer Mohammed on 12/5/2014.
 */
public class IncidentsByLocation extends Activity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incidentsbylocation);
        Button back_button = (Button) findViewById(R.id.back_button);
        back_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent next_intent = new Intent (getApplicationContext(),PhoneComponent.class); //cal vs oregon class
        startActivity(next_intent);
    }

}
