package edu.berkeley.cs160.woqytoqy.woqytoqy;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DeveloperFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DeveloperFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class DeveloperFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DeveloperFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeveloperFragment newInstance(String param1, String param2) {
        DeveloperFragment fragment = new DeveloperFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public DeveloperFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().setTitle("DEVELOPER OPTIONS");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_developer, container, false);

        final Spinner severity_spinner = (Spinner) v.findViewById(R.id.severity_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> severity_adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.severity_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        severity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        severity_spinner.setAdapter(severity_adapter);

        final Spinner filled_people_spinner = (Spinner) v.findViewById(R.id.filled_people_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> filled_people_adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.filled_people_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        filled_people_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        filled_people_spinner.setAdapter(filled_people_adapter);

        Button installBtn = (Button)v.findViewById(R.id.install);
        installBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onInstallSelected();
            }
        });
        Button uninstallBtn = (Button)v.findViewById(R.id.uninstall);
        uninstallBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onUninstallSelected();
            }
        });

        Button filledPeopleBtn = (Button)v.findViewById(R.id.filled_people);
        filledPeopleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int filled_people = Integer.valueOf(filled_people_spinner.getSelectedItem().toString());
                onFilledPeopleSelected(filled_people);
            }
        });
        Button requestReceivedBtn = (Button)v.findViewById(R.id.request_received);
        requestReceivedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String severity = severity_spinner.getSelectedItem().toString();
                onRequestReceivedSelected(severity);
            }
        });
        Button requestResolvedBtn = (Button)v.findViewById(R.id.request_resolved);
        requestResolvedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRequestResolvedSelected();
            }
        });
        Button requestDeclinedBtn = (Button)v.findViewById(R.id.request_declined);
        requestDeclinedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRequestDeclinedSelected();
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onRequestReceivedSelected(String severity) {
        if (mListener != null) {
            mListener.requestReceived(severity);
        }
    }

    public void onRequestResolvedSelected() {
        if (mListener != null) {
            mListener.requestResolved();
        }
    }

    public void onRequestDeclinedSelected() {
        if (mListener != null) {
            mListener.requestDeclined();
        }
    }

    public void onFilledPeopleSelected(int filled_people){
        if (mListener != null) {
            mListener.requestFilledPeople(filled_people);
        }
    }

    public void onInstallSelected(){
        if (mListener != null) {
            mListener.requestInstall();
        }
    }

    public void onUninstallSelected(){
        if (mListener != null) {
            mListener.requestUninstall();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void requestResolved();
        public void requestDeclined();
        public void requestReceived(String severity);
        public void requestFilledPeople(int filled_people);
        public void requestInstall();
        public void requestUninstall();
    }

}
