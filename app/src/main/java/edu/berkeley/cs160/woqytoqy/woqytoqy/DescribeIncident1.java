package edu.berkeley.cs160.woqytoqy.woqytoqy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Sumer Mohammed on 12/5/2014.
 */
public class DescribeIncident1 extends Activity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.describeincident1);
        Button back_button = (Button) findViewById(R.id.back_button);
        back_button.setOnClickListener(this);
        final Button black_button = (Button) findViewById(R.id.black_button);
        black_button.setVisibility(View.GONE);
        final Button description_invisible = (Button) findViewById(R.id.description_invisible);
        final EditText description_button = (EditText) findViewById(R.id.edit_description);

        description_button.setVisibility(View.GONE);
        description_invisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                black_button.setVisibility(View.VISIBLE);
                description_button.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent next_intent = new Intent (getApplicationContext(),IncidentList.class);
        startActivity(next_intent);
    }
}
