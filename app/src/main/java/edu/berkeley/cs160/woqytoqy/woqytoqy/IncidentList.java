package edu.berkeley.cs160.woqytoqy.woqytoqy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Sumer Mohammed on 12/4/2014.
 */
public class IncidentList extends Activity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incident_list);
        Button sectionc4_button = (Button) findViewById(R.id.sectionc4);
        Button back_button = (Button) findViewById(R.id.back_button);
        sectionc4_button.setOnClickListener(this);
        back_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sectionc4) {
            Intent next_intent = new Intent(getApplicationContext(), DescribeIncident1.class);
            startActivity(next_intent);
        } else if (view.getId() == R.id.back_button) {
            Intent next_intent = new Intent(getApplicationContext(), PhoneComponent.class); //cal vs oregon class
            startActivity(next_intent);
        }
    }

}
