package edu.berkeley.cs160.woqytoqy.woqytoqy;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.MenuOption;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.DeckOfCardsLauncherIcon;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.view.View;
import android.widget.Button;

public class MainActivity extends FragmentActivity implements View.OnClickListener, DeveloperFragment.OnFragmentInteractionListener {


    public int current_main;
    public String location = "C10";
    public String severity;
    public String time;
    public String scenario;
    public int num_people;
    public int filled_people;

    private final static String PREFS_FILE= "prefs_file";
    private final static String DECK_OF_CARDS_KEY= "deck_of_cards_key";
    private final static String DECK_OF_CARDS_VERSION_KEY= "deck_of_cards_version_key";

    private DeckOfCardsManager mDeckOfCardsManager;
    private DeckOfCardsEventListener deckOfCardsEventListener;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;
    private CardImage[] mCardImages;
    private ListCard listCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button developer_screen = (Button)  findViewById(R.id.developer_screen);
        developer_screen.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                DialogFragment newFragment = DeveloperFragment.newInstance("", "");
                newFragment.show(getSupportFragmentManager(), "dialog");
                return true;
            }
        });

        Button events_screen = (Button) findViewById(R.id.events_screen);
        events_screen.setOnClickListener(this);

        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        deckOfCardsEventListener= new DeckOfCardsEventListenerImpl();
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * @see android.app.Activity#onStart()
     * This is called after onCreate(Bundle) or after onRestart() if the activity has been stopped
     */
    protected void onStart() {
        super.onStart();
        mDeckOfCardsManager.addDeckOfCardsEventListener(deckOfCardsEventListener);
        Log.d(Constants.TAG, "ToqApiDemo.onStart");
        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }
    }

    // Initialise
    private void init(){

        // Create the resource store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();

        DeckOfCardsLauncherIcon colorIcon = null;

        // Get the launcher icons
        try{
            colorIcon= new DeckOfCardsLauncherIcon("color.launcher.icon", getBitmap("woqy_toqy_icon.png"), DeckOfCardsLauncherIcon.COLOR);
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Can't get launcher icon");
            return;
        }

        mCardImages = new CardImage[4];
        try{
            mCardImages[0]= new CardImage("four_empty", getPeopleBitmap("four-empty_text.png"));
            mCardImages[1]= new CardImage("four_one_filled", getPeopleBitmap("one_filled_three_empty_text.png"));
            mCardImages[2]= new CardImage("five_empty", getPeopleBitmap("five-empty_text.png"));
            mCardImages[3]= new CardImage("five_one_filled", getPeopleBitmap("one_filled_four_empty_text.png"));
            mRemoteResourceStore.addResource(mCardImages[0]);
            mRemoteResourceStore.addResource(mCardImages[1]);
            mRemoteResourceStore.addResource(mCardImages[2]);
            mRemoteResourceStore.addResource(mCardImages[3]);
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Can't get picture");
            return;
        }

        // Make sure in usable state
        if (mRemoteDeckOfCards == null){
            mRemoteDeckOfCards = createBackupRequestDeckOfCards();
        }

        // Set the custom launcher icons, adding them to the resource store
        mRemoteResourceStore.addResource(colorIcon);
        mRemoteDeckOfCards.setLauncherIcons(mRemoteResourceStore, new DeckOfCardsLauncherIcon[]{colorIcon});
    }

    @Override
    public void onClick(View view) {
        Intent first_intent = new Intent(getApplicationContext(), PhoneComponent.class);
        startActivity(first_intent);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_install) {
//            install();
//        } else if (id == R.id.action_uninstall){
//            uninstall();
//        } else if (id == R.id.action_developer) {
//            DialogFragment newFragment = DeveloperFragment.newInstance("", "");
//            newFragment.show(getSupportFragmentManager(), "dialog");
//        }
//        return super.onOptionsItemSelected(item);
//    }

    // Create some cards with example content
    private RemoteDeckOfCards createBackupRequestDeckOfCards(){
        Log.d("Remote deck of cards", "");
        listCard = new ListCard();
        num_people = 4;
        filled_people = 0;
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.backup_request));
        simpleTextCard.setTitleText("Current Location: " + location);
        simpleTextCard.setMessageText(new String[]{"𩦢                                                    𩦢"});
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{
                new MenuOption("Critical", false),
                new MenuOption("High", false)
        });
        listCard.add(simpleTextCard);
        current_main = R.string.backup_request;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createSeverityDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.select_severity));
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{
                new MenuOption("Critical", false),
                new MenuOption("High", false),
                new MenuOption("Medium", false),
                new MenuOption("Low", false),
                new MenuOption("Cancel", false)
        });
        listCard.add(simpleTextCard);
        current_main = R.string.severity;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createRequestSentDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.request_sent));
        simpleTextCard.setTitleText("Severity: " + severity);
        if (num_people == 4 && filled_people == 0){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[0]);
        }
        else if (num_people == 4 && filled_people == 1){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[1]);
        }
        else if (num_people == 5 && filled_people == 0){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[2]);
        }
        else if (num_people == 5 && filled_people == 1){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[3]);
        }
//        String[] messages = {"Location: " + location, "Resending in: 0:30"};
//        simpleTextCard.setMessageText(messages);
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{
                                                          new MenuOption(getString(R.string.more_backup), false),
                                                          new MenuOption(getString(R.string.resolve_incident), false),
                                                          new MenuOption("Cancel Request", false)}
        );
        listCard.add(simpleTextCard);
        current_main = R.string.request_sent;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createRequestResentDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.request_resent));
        simpleTextCard.setTitleText("Severity: " + severity);
        if (num_people == 4 && filled_people == 0){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[0]);
        }
        else if (num_people == 4 && filled_people == 1){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[1]);
        }
        else if (num_people == 5 && filled_people == 0){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[2]);
        }
        else if (num_people == 5 && filled_people == 1){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[3]);
        }
//        String[] messages = {"Location: " + location, "Resending in: 0:30"};
//        simpleTextCard.setMessageText(messages);
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{
                        new MenuOption(getString(R.string.more_backup), false),
                        new MenuOption(getString(R.string.resolve_incident), false),
                        new MenuOption("Cancel Request", false)}
        );
        listCard.add(simpleTextCard);
        current_main = R.string.request_resent;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createRequestDetailsDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.request_details));
        simpleTextCard.setTitleText("Severity: " + severity);
        if (num_people == 4 && filled_people == 0){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[0]);
        }
        else if (num_people == 4 && filled_people == 1){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[1]);
        }
        else if (num_people == 5 && filled_people == 0){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[2]);
        }
        else if (num_people == 5 && filled_people == 1){
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[3]);
        }
//        String[] messages = {"Location: " + location, "Resending in: 0:30"};
//        simpleTextCard.setMessageText(messages);
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{
                        new MenuOption(getString(R.string.more_backup), false),
                        new MenuOption(getString(R.string.resolve_incident), false),
                        new MenuOption("Cancel Request", false)}
        );
        listCard.add(simpleTextCard);
        current_main = R.string.request_details;
        return new RemoteDeckOfCards(this, listCard);
    }


    private RemoteDeckOfCards createRequestCancelDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText("Backup Cancelled");
        simpleTextCard.setTitleText("Swipe right to return to the main menu");
        String[] messages = {"Tip: Cancel backup requests by swiping right."};
        simpleTextCard.setMessageText(messages);
        listCard.add(simpleTextCard);
        current_main = R.string.request_cancel;
        return new RemoteDeckOfCards(this, listCard);
    }

    //Not done
    private RemoteDeckOfCards createResolveIncidentDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.resolve_incident));
        simpleTextCard.setMenuOptionObjs(
                new MenuOption[]{
                        new MenuOption("Crowd Control", false),
                        new MenuOption("Fight", false),
                        new MenuOption("Med. Emergency", false),
                        new MenuOption("Other", false),
                        new MenuOption("Cancel", false)
                }
        );
        listCard.add(simpleTextCard);
        current_main = R.string.resolve_incident;

        return new RemoteDeckOfCards(this, listCard);
    }

    //A screen that only pops up for a couple of seconds
    private RemoteDeckOfCards createResendNowDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(false);
        simpleTextCard.setHeaderText("Backup Requests Resent");
        listCard.add(simpleTextCard);
        current_main = R.string.resend_now;

        return new RemoteDeckOfCards(this, listCard);
    }

    //A screen that only pops up for a couple of seconds
    private RemoteDeckOfCards createMoreBackupDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(false);
        simpleTextCard.setHeaderText("Requesting More Backup...");
        listCard.add(simpleTextCard);
        current_main = R.string.more_backup;

        return new RemoteDeckOfCards(this, listCard);
    }

    //A screen that only pops up for a couple of seconds
    private RemoteDeckOfCards createMaxBackupDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(false);
        simpleTextCard.setHeaderText("Maximum Backup Requests");
        simpleTextCard.setTitleText("All guards have already been notified");
        listCard.add(simpleTextCard);
        current_main = R.string.max_requests;

        return new RemoteDeckOfCards(this, listCard);
    }

    //A screen that only pops up for a couple of seconds
    private RemoteDeckOfCards createSendingRequestDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(false);
        simpleTextCard.setHeaderText("Requesting Backup...");
        listCard.add(simpleTextCard);
        current_main = R.string.sending_request;

        return new RemoteDeckOfCards(this, listCard);
    }

    //A screen that only pops up for a couple of seconds
    private RemoteDeckOfCards createCancelDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(false);
        simpleTextCard.setHeaderText(getString(R.string.cancel));
        listCard.add(simpleTextCard);
        current_main = R.string.cancel;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createRequestReceivedDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.request_received));
        simpleTextCard.setTitleText("Severity: " + severity);
        String[] messages = {"Location: " + location, "Received: " + time};
        simpleTextCard.setMessageText(messages);
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{new MenuOption("Accept", false), new MenuOption("Decline", false)});
        listCard.add(simpleTextCard);
        current_main = R.string.request_received;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createRequestAcceptedDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText(getString(R.string.request_accepted));
        simpleTextCard.setTitleText("Severity: " + severity);
        String[] messages = {"Location: " + location, "Received: " + time};
        simpleTextCard.setMessageText(messages);
        listCard.add(simpleTextCard);
        current_main = R.string.request_accepted;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createRequestDeclinedDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText("Request Declined");
        simpleTextCard.setTitleText("Swipe back to return to the main menu");
        listCard.add(simpleTextCard);
        current_main = R.string.request_declined;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createConfirmationDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setTitleText("Are you sure you want to resolve the incident?");
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{new MenuOption("Yes", false), new MenuOption("No", false)});
        listCard.add(simpleTextCard);
        current_main = R.string.confirmation;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createFinishedDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText("Incident Resolved");
        simpleTextCard.setTitleText("Backup No Longer Needed");
        simpleTextCard.setMessageText(new String[]{"Swipe right to return to the main menu"});
        listCard.add(simpleTextCard);
        current_main = R.string.finished;
        return new RemoteDeckOfCards(this, listCard);
    }

    private RemoteDeckOfCards createDeclinedDeckOfCards(){
        listCard = new ListCard();
        SimpleTextCard simpleTextCard = new SimpleTextCard(getString(R.string.main));
        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setHeaderText("Requests Declined");
        simpleTextCard.setTitleText("You're on your own");
        simpleTextCard.setMessageText(new String[]{"Everyone declined your request your help."});
        simpleTextCard.setMenuOptionObjs(new MenuOption[]{new MenuOption("Ask again", false), new MenuOption("I got this", false)});
        listCard.add(simpleTextCard);
        current_main = R.string.backup_declined;
        return new RemoteDeckOfCards(this, listCard);
    }

    @Override
    public void requestResolved() {
        try {
            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createFinishedDeckOfCards(), mRemoteResourceStore);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestDeclined() {
        try {
            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createDeclinedDeckOfCards(), mRemoteResourceStore);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestReceived(String severity) {
        try {
            // Send notification
            // Need to get rid of seconds
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
            time = dateFormat.format(new Date()).toString();
            this.severity = severity;

            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestReceivedDeckOfCards(), mRemoteResourceStore);
            RemoteToqNotification remoteToqNotification = new RemoteToqNotification(
                    getApplicationContext(),
                    (long) 1.0, "Backup Requested",
                    new String[]{"Check WoqyToqy to accept or decline"});
            mDeckOfCardsManager.sendNotification(remoteToqNotification);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestFilledPeople(int filled_people) {
        this.filled_people = filled_people;
        try {
            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void requestInstall() {
       install();
    }

    @Override
    public void requestUninstall() {
        uninstall();
            //DialogFragment newFragment = DeveloperFragment.newInstance("", "");
            //newFragment.show(getSupportFragmentManager(), "dialog");
    }

    // Handle card events triggered by the user interacting with a card in the installed deck of cards
    private class DeckOfCardsEventListenerImpl implements DeckOfCardsEventListener {

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardOpen(java.lang.String)
         */
        public void onCardOpen(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardVisible(java.lang.String)
         */
        public void onCardVisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    if (current_main == R.string.backup_request){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createSeverityDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    //Toast.makeText(MainActivity.this, "R.string.event_card_visible", Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardInvisible(java.lang.String)
         */
        public void onCardInvisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){

                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardClosed(java.lang.String)
         */
        public void onCardClosed(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    if (current_main == R.string.severity){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createBackupRequestDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.request_sent || current_main == R.string.request_details || current_main == R.string.request_resent || current_main == R.string.sending_request){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createCancelDeckOfCards(), mRemoteResourceStore);

                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createBackupRequestDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.request_details){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createCancelDeckOfCards(), mRemoteResourceStore);

                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createBackupRequestDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.resolve_incident){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.finished){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createBackupRequestDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.confirmation){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.backup_declined){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.request_declined){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createBackupRequestDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.request_cancel){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createBackupRequestDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.max_requests){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (current_main == R.string.more_backup){
                        try {
                            mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                        } catch (RemoteDeckOfCardsException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(java.lang.String, java.lang.String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption){
            runOnUiThread(new Runnable(){
                public void run(){
                    if (current_main == R.string.severity){
                        if (menuOption.equals("Cancel")){
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestCancelDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createSendingRequestDeckOfCards(), mRemoteResourceStore);

                                severity = menuOption;
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestSentDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (current_main == R.string.request_sent || current_main == R.string.request_details || current_main == R.string.request_resent){
                        if (menuOption.equals(getString(R.string.resolve_incident))) {
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createResolveIncidentDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }

                        else if (menuOption.equals(getString(R.string.more_backup))) {
                            // More backup, increase the num_people by one.
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMoreBackupDeckOfCards(), mRemoteResourceStore);
                                if (num_people == 4){
                                    num_people = 5;
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestSentDeckOfCards(), mRemoteResourceStore);
                                }
                                else {
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createMaxBackupDeckOfCards(), mRemoteResourceStore);
                                    mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                                }
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                        else if (menuOption.equals("Cancel Request")) {
                            // Resend Now resends the request to all security guards that have been asked and have not acknowledged the request yet.
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestCancelDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (current_main == R.string.resolve_incident){
                        if (menuOption.equals("Cancel")){
                            try {
                                scenario = menuOption;
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                scenario = menuOption;
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createConfirmationDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (current_main == R.string.confirmation){
                        if (menuOption.equals("No")){
                            scenario = null;
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                        else if (menuOption.equals("Yes")){
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createFinishedDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (current_main == R.string.request_received){
                        if (menuOption.equals("Accept")){
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestAcceptedDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                        else if (menuOption.equals("Decline")){
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDeclinedDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else if (current_main == R.string.backup_declined){
                        if (menuOption.equals("Ask again")) {
                            // Ask again restarts the process because everyone has declined.
                            try {
                                severity = null;
                                scenario = null;
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createSeverityDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                        else if (menuOption.equals("I got this")){
                            try {
                                mDeckOfCardsManager.updateDeckOfCards(MainActivity.this.createRequestDetailsDeckOfCards(), mRemoteResourceStore);
                            } catch (RemoteDeckOfCardsException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(java.lang.String, java.lang.String, java.lang.String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption, final String quickReplyOption){
            runOnUiThread(new Runnable(){
                public void run(){
                    Toast.makeText(MainActivity.this, "R.string.event_menu_option_selected", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    /**
     * Installs applet to Toq watch if app is not yet installed
     */
    private void install() {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            if (e.getErrorCode() == -101){
                Toast.makeText(this, "Please connect with the Toq smart watch.", Toast.LENGTH_LONG).show();
            }
        }

        if (!isInstalled) {
            try {
                mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
            } catch (RemoteDeckOfCardsException e) {
                if (e.getErrorCode() == -101){
                    Toast.makeText(this, "Please connect with the Toq smart watch to install.", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast toast = Toast.makeText(this, "App is already installed!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }


    }

    private void uninstall() {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }

        if (isInstalled) {
            try{
                mDeckOfCardsManager.uninstallDeckOfCards();
                Toast.makeText(this, "Uninstalled", Toast.LENGTH_SHORT).show();
            }
            catch (RemoteDeckOfCardsException e){
                if (e.getErrorCode() == -101){
                    Toast.makeText(this, "Please connect with the Toq smart watch to uninstall.", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(this, "Already uninstalled", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isInstalled(){
        try {
            return mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            if (e.getErrorCode() == -101){
                Toast.makeText(this, "Please connect with the Toq smart watch.", Toast.LENGTH_LONG).show();
            }
            return false;
        }
    }

    public boolean isConnected(){
        try {
            return mDeckOfCardsManager.isToqWatchConnected();
        }
        catch (RemoteDeckOfCardsException e) {
            Toast.makeText(getApplicationContext(), "Error: Can't determine if watch is connected.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    // Read an image from assets and return as a bitmap
    private Bitmap getBitmap(String fileName) throws Exception{

        try{
            InputStream is= getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }

    }

    private Bitmap getPeopleBitmap(String fileName) throws Exception {
        try {
            InputStream is = getAssets().open(fileName);
            return Bitmap.createScaledBitmap(BitmapFactory.decodeStream(is), 250, 288, false);
        } catch (Exception e) {
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }
}

